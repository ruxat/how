package cycTrain;

public class Carriage {
    private boolean lampIsOn;

    public Carriage(boolean lampIsOn) {
        this.lampIsOn = lampIsOn;
    }

    public boolean getLampIsOn() {
        return lampIsOn;
    }

    public void setLampIsOn(boolean lampIsOn) {
        this.lampIsOn = lampIsOn;
    }
}
