package cycTrain;

import java.util.ArrayList;
import java.util.Random;

public class Train {
    // final int MAX_NUMBER = 20;
    private int trainLength, humanPos;
//    private Carriage[] carriages;
    private ArrayList<Carriage> carriages = new ArrayList<Carriage>();
    Random random = new Random();


// Train constructing with adding a fixed number of carriages
// and random lamp switch positions in each


    public Train() {
        trainLength = 3100345;
        humanPos = Math.abs(random.nextInt()) % trainLength;
//        carriages = new Carriage[trainLength];
        for (int i = 0; i < trainLength; i++)
            carriages.add(new Carriage(random.nextBoolean()));
//        System.out.println("I am a man in a train that has from 1 to " + MAX_NUMBER + " carriages");
        System.out.println("I am a man in a train that has " + trainLength + " carriages");
    }

//    A story about the train current state
    public void tell(){
        System.out.println("The train has " + trainLength + " carriages");
        if (trainLength < 21)
            for (int i = 0; i < trainLength; i++)
                System.out.println(i + ") " + (carriages.get(i).getLampIsOn() ? "LIGHT" : "dark") + ((i == humanPos)?" <== I am here":""));
    }

    public void moveBy(int delta) {
        humanPos = (humanPos + delta + trainLength) % trainLength;
    }

//    Check if it's light around the man
    public boolean isLight() {
        return carriages.get(humanPos).getLampIsOn();
    }

    public void moveLeft() {
        if (--humanPos < 0) humanPos += trainLength;
    }

    public void moveRight() {
        if (++humanPos >= trainLength) humanPos = 0;
    }

    public void setLamp(boolean isOn) {
        carriages.get(humanPos).setLampIsOn(isOn);
    }
}
