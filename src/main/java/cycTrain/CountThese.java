package cycTrain;

public class CountThese {
    public static void main(String[] args) {
        System.out.println("hi!");
        Train train = new Train();

        // Initialising
        int i, n = 1;
        boolean direction = true;
        train.setLamp(false);

        // Loop starts
        while (true) {

            // Turn n lamps off and one on
            for (i = 0; i <= n; i++) {
                moveTo(train, direction);
                train.setLamp(false);
            }
            train.setLamp(true);

            // Change direction
            direction = !direction;

            // Double n
            n <<= 1;
            System.out.println("I check light in " + n + " carriages in " + (direction?"right":"left") + " direction");

            // Check for light in the n carriages
            for (i = 1; i <= n; i++) {
                moveTo(train, direction);
                if (train.isLight()) break;
            }

            // Loop ends
            if (i < n) break;
        }

        // Show the results
        System.out.println("I think, the number of carriages is " + i);

        System.out.print("The right answer is: ");
        train.tell();
    }

    private static void moveTo(Train train, boolean direction) {
        if (direction) train.moveRight();
        else train.moveLeft();
    }
}
